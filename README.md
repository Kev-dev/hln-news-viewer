# README #

## Simple news feed scraper made in Node ##

#### Since I was reading HLN on a daily basis I decided to make a scraper for it. The results are displayed on a page ####

### [View demo](https://hln-news-viewer.herokuapp.com) ###
### [View scraped data](https://hln-news-viewer.herokuapp.com/hln) ###

### Technologies used: ###
* Node
* Express
* Request
* Cheerio
* Axios 

