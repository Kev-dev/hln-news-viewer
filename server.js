const express = require("express");
const app = express();
const PORT = process.env.PORT || 1337;

const cheerio = require("cheerio");
const request = require("request");

const refreshTime = 60000; //miliseconds

const HLN = "http://www.hln.be";
let newsItems = [];

function addNewsItem(title, hour, excerpt, article_url) {
    let newsItem = {
        title,
        hour,
        excerpt,
        article_url
    }
    newsItems.push(newsItem);
}

function fetchDataFromHln() {
    request(HLN, (err, res, html) => {
        if (!err && res.statusCode === 200) {
            newsItems = [];
            let $ = cheerio.load(html);

            // Grab the headline news title
            let title = $("section.articleblock_1").find("h1").text();

            // Grab the hour for the article
            let hour = $("section.articleblock_1").find(".artlarge .time").text();

            // Grab the excerpt for the headline article
            let excerpt = $("section.articleblock_1").find(".artlarge p a").contents().filter(function () {
                return this.type != 'tag';
            }).text().trim();

            // Find the url of the article
            let url = HLN + $("section.articleblock_1").find(".artlarge p a").attr('href');
            addNewsItem(title, hour, excerpt, url);

            $("article[class='article']").each(function (i, elm) {
                // Grab all news titles.
                let title = $(this).find("h3").text();

                let hour = $(this).find(".time").text();

                let excerpt = $(this).find("p a").contents().filter(function () {
                    return this.type === "text";
                }).text().trim();

                let url = HLN + $(this).find("p a").attr("href");

                addNewsItem(title, hour, excerpt, url);
            });
        }
    })
}

setInterval(fetchDataFromHln, refreshTime);
fetchDataFromHln();

app.use('/axios', express.static(__dirname + '/node_modules/axios/dist/'));
app.use(express.static('public'))


app.get("/", (req, res) => {
    res.sendFile(__dirname + "/index.html");
})

app.get("/hln", (req, res) => {
    res.send(newsItems);
})


app.listen(PORT);

