const LOGO = "http://static0.hln.be/images/logos/hln_logo.png?7.5.0.20170602";

(function setLogo() {
  document.getElementById("logo").src = LOGO;
  document.getElementById("logo").alt = "hln logo"
})();

axios.get('/hln').then((res) => {
  let data = res.data;
  for (let i = 0; i < data.length; i++) {
    let newsItem = document.createElement("div");
    newsItem.className = "row news-item";

    let hour = document.createElement("div");
    hour.className = "col-xs-12 col-sm-1 col-md-offset-3 col-sm-offset-1 hour";
    hour.innerText = data[i].hour;

    let newsTitle = document.createElement("div");
    newsTitle.className = "col-xs-12 col-md-5 col-sm-9 news-title";
    newsTitle.innerText = data[i].title;

    let excerpt = document.createElement("div");
    excerpt.className = "col-md-5 col-md-offset-4 col-sm-9 col-sm-offset-2 news-excerpt";
    excerpt.innerText = data[i].excerpt;

    let breakDiv = document.createElement("div");
    breakDiv.className = "visible-xs mobile-break";
    breakDiv.innerHTML = "<br>";
    excerpt.appendChild(breakDiv);

    let readMore = document.createElement("a");
    readMore.innerText = " (Klik hier om verder te lezen op hln.be)";
    readMore.href = data[i].article_url;
    excerpt.appendChild(readMore);

    if (i != data.length - 1) {
      let redLine = document.createElement("div");
      redLine.className = "red-line";
      excerpt.appendChild(redLine);
    }

    newsItem.appendChild(hour);
    newsItem.appendChild(newsTitle);
    newsItem.appendChild(excerpt);

    document.getElementById("news").appendChild(newsItem);
  }
})



